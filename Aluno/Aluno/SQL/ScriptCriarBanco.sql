﻿CREATE DATABASE Cadastro
GO
USE [Cadastro]
GO

CREATE TABLE [dbo].[Aluno](
[Id] [int] IDENTITY(1,1) Primary key,
[Nome] [nvarchar](50) NOT NULL,
[Email] [nvarchar](100) NOT NULL,
[Curso] [nvarchar](50) NULL,
[Sexo] [nvarchar](50) NULL,
)
