﻿using Aluno.Models;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Aluno.Controllers
{
    public class AlunoController : Controller
    {
        // GET: Aluno
        public ActionResult Index()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var aluno = session.Query<Aluno.Models.Aluno>().ToList();
                return View(aluno);
            }
        }
        [HttpGet]
        public ActionResult NovoAluno()
        {
            return View();
        }
        [HttpPost]
        public ActionResult NovoAluno(Aluno.Models.Aluno aluno)
        {
            try
            {
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(aluno);
                        transaction.Commit();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ViewBag.Erro = ("Falha na Operação favor entrar em contato com o suporte");
                return View();
            }
        }

        [HttpGet]
        public ActionResult AlterarAluno(int id)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var aluno = session.Get<Aluno.Models.Aluno>(id);
                return View(aluno);
            }
        }
        [HttpPost]
        public ActionResult AlterarAluno(int id, Aluno.Models.Aluno aluno)
        {
            try
            {
                using (ISession session =NHibernateHelper.OpenSession())
                {
                    var AlunoAlterado = session.Get<Aluno.Models.Aluno>(id);
                    AlunoAlterado.Nome = aluno.Nome;
                    AlunoAlterado.Email = aluno.Email;
                    AlunoAlterado.Curso = aluno.Curso;
                    AlunoAlterado.Sexo = aluno.Sexo;
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(AlunoAlterado);
                        transaction.Commit();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ViewBag.Erro = ("Falha na Operação favor entrar em contato com o suporte");
                return View();
            }
        }
        [HttpGet]
        public ActionResult DeletarAluno(int id)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var aluno = session.Get<Aluno.Models.Aluno>(id);
                return View(aluno);
            }
        }
        [HttpPost]
        public ActionResult DeletarAluno(int id, Aluno.Models.Aluno aluno)
        {
            try
            {
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Delete(aluno);
                        transaction.Commit();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ViewBag.Erro = ("Falha na Operação favor entrar em contato com o suporte");
                return View();
            }
        }
    }
}